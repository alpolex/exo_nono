(function(){
    'use strict';

    let todos = [];

    const myTodo = function (id){
        this.id = id;
        this.items = [];
        this.addItem = addItem;
        this.removeItems = removeItems;
        this.selectItem = selectItem;
        this.getItems = ()=>{ return this.items };

        function addItem(content){
            this.items.push({
                "content":content,
                "done": false
            });

            return this.items;
        }

        function removeItems(e){
            const items = document.querySelectorAll('.item');

            for (var i = 0; i < this.items.length; i++) {
                if (this.items[i].done) {
                    this.items.splice(i, 1);

                    for (var i = 0; i < items.length; i++) {
                        if (items[i].textContent === this.items[i].content) {
                            items[i].parentElement.removeChild(items[i]);
                        }
                    }
                }
            }

            return this.items;
        }

        function selectItem(e){
            for (let i = 0; i < this.items.length; i++) {
                if (e.target.textContent === this.items[i].content) {
                    this.items[i].done = !this.items[i].done;
                }
            }
        }
    };

    const todosUpdate = ()=>{
        let elemList = document.querySelectorAll('.todo');

        for (let i = 0; i < elemList.length; i++) {
            todos.push(new myTodo(elemList[i].id))
        }
    };

    const todosEvents = ()=>{
        for (let i = 0; i < todos.length; i++) {
            let addBtn = document.querySelector('#button-add' + (i+1));
            let clearBtn = document.querySelector('#button-clear' + (i+1));
            let addField = document.querySelector('#field-add' + (i+1));
            let listItem = document.querySelector('#item-list' + (i+1));

            addBtn.addEventListener('click', ()=>{
                let item = document.createElement('li');

                item.classList.add('item');
                item.innerText = addField.value;
                listItem.appendChild(item);
                todos[i].addItem(addField.value);

                for (let j = 0; j < listItem.childNodes.length; j++) {
                    listItem.childNodes[j].addEventListener('click', (e)=>{
                        todos[i].selectItem(e);
                    });
                }
            });

            clearBtn.addEventListener('click', ()=>{
                todos[i].removeItems();
            });
        }
    };

    const todosInit = ()=>{
        todosUpdate();
        todosEvents();
    };

    return todosInit();
})();
